

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class LocationController : Controller
    {
        public LocationController(LocationSvc locationSvc)

        {
            _locationSvc = locationSvc;
        }
        private LocationSvc _locationSvc;


        //
        // GET: /Location/

        public ActionResult Index()
        {
            return View(new LocationList(_locationSvc.GetAll()));
        }

        //
        // GET: /Location/Details/5

        public ActionResult Details(int id = 0)
        {
            Location location = _locationSvc.GetById(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            LocationVm locationVm = new LocationVm(location);





            return View(locationVm);
        }

        //
        // GET: /Location/Create

        public ActionResult Create(
        )
        {
                Location location = new Location();
LocationVm locationVm = new LocationVm(location);
            return View(locationVm);
        }

        //
        // POST: /Location/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LocationVm locationVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _locationSvc.Update(locationVm.Location);
                return RedirectToAction("Index");
            }

            return View(locationVm);
        }

        //
        // GET: /Location/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Location location = _locationSvc.GetById(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            LocationVm locationVm = new LocationVm(location);
            return View(locationVm);
        }

        //
        // POST: /Location/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LocationVm locationVm)
        {
            if (ModelState.IsValid)
            {
                _locationSvc.Update(locationVm.Location);
                return RedirectToAction("Index");
            }
            return View(locationVm);
        }

        //
        // GET: /Location/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Location location = _locationSvc.GetById(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            LocationVm locationVm = new LocationVm(location);
            return View(locationVm);
        }

        //
        // POST: /Location/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _locationSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}