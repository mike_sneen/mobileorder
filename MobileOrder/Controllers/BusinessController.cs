

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class BusinessController : Controller
    {
        public BusinessController(BusinessSvc businessSvc)

        {
            _businessSvc = businessSvc;
        }
        private BusinessSvc _businessSvc;


        //
        // GET: /Business/

        public ActionResult Index()
        {
            return View(new BusinessList(_businessSvc.GetAll()));
        }

        //
        // GET: /Business/Details/5

        public ActionResult Details(int id = 0)
        {
            Business business = _businessSvc.GetById(id);
            if (business == null)
            {
                return HttpNotFound();
            }
            BusinessVm businessVm = new BusinessVm(business);





            return View(businessVm);
        }

        //
        // GET: /Business/Create

        public ActionResult Create(
        )
        {
                Business business = new Business();
BusinessVm businessVm = new BusinessVm(business);
            return View(businessVm);
        }

        //
        // POST: /Business/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BusinessVm businessVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _businessSvc.Update(businessVm.Business);
                return RedirectToAction("Index");
            }

            return View(businessVm);
        }

        //
        // GET: /Business/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Business business = _businessSvc.GetById(id);
            if (business == null)
            {
                return HttpNotFound();
            }
            BusinessVm businessVm = new BusinessVm(business);
            return View(businessVm);
        }

        //
        // POST: /Business/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BusinessVm businessVm)
        {
            if (ModelState.IsValid)
            {
                _businessSvc.Update(businessVm.Business);
                return RedirectToAction("Index");
            }
            return View(businessVm);
        }

        //
        // GET: /Business/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Business business = _businessSvc.GetById(id);
            if (business == null)
            {
                return HttpNotFound();
            }
            BusinessVm businessVm = new BusinessVm(business);
            return View(businessVm);
        }

        //
        // POST: /Business/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _businessSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}