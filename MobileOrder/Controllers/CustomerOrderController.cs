

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class CustomerOrderController : Controller
    {
        public CustomerOrderController(CustomerOrderSvc customerOrderSvc)

        {
            _customerOrderSvc = customerOrderSvc;
        }
        private CustomerOrderSvc _customerOrderSvc;


        //
        // GET: /CustomerOrder/

        public ActionResult Index()
        {
            return View(new CustomerOrderList(_customerOrderSvc.GetAll()));
        }

        //
        // GET: /CustomerOrder/Details/5

        public ActionResult Details(int id = 0)
        {
            CustomerOrder customerOrder = _customerOrderSvc.GetById(id);
            if (customerOrder == null)
            {
                return HttpNotFound();
            }
            CustomerOrderVm customerOrderVm = new CustomerOrderVm(customerOrder);





            return View(customerOrderVm);
        }

        //
        // GET: /CustomerOrder/Create

        public ActionResult Create(
            int? CustomerId,            int? LocationId        )
        {
                CustomerOrder customerOrder = new CustomerOrder();
                if (CustomerId.HasValue)
               {
                    customerOrder.CustomerId = CustomerId.Value;
               }
                if (LocationId.HasValue)
               {
                    customerOrder.LocationId = LocationId.Value;
               }
CustomerOrderVm customerOrderVm = new CustomerOrderVm(customerOrder);
            return View(customerOrderVm);
        }

        //
        // POST: /CustomerOrder/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerOrderVm customerOrderVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _customerOrderSvc.Update(customerOrderVm.CustomerOrder);
                return RedirectToAction("Index");
            }

            return View(customerOrderVm);
        }

        //
        // GET: /CustomerOrder/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CustomerOrder customerOrder = _customerOrderSvc.GetById(id);
            if (customerOrder == null)
            {
                return HttpNotFound();
            }
            CustomerOrderVm customerOrderVm = new CustomerOrderVm(customerOrder);
            return View(customerOrderVm);
        }

        //
        // POST: /CustomerOrder/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerOrderVm customerOrderVm)
        {
            if (ModelState.IsValid)
            {
                _customerOrderSvc.Update(customerOrderVm.CustomerOrder);
                return RedirectToAction("Index");
            }
            return View(customerOrderVm);
        }

        //
        // GET: /CustomerOrder/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CustomerOrder customerOrder = _customerOrderSvc.GetById(id);
            if (customerOrder == null)
            {
                return HttpNotFound();
            }
            CustomerOrderVm customerOrderVm = new CustomerOrderVm(customerOrder);
            return View(customerOrderVm);
        }

        //
        // POST: /CustomerOrder/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _customerOrderSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}