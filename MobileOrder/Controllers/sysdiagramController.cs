

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class sysdiagramController : Controller
    {
        public sysdiagramController(sysdiagramSvc sysdiagramSvc)

        {
            _sysdiagramSvc = sysdiagramSvc;
        }
        private sysdiagramSvc _sysdiagramSvc;


        //
        // GET: /sysdiagram/

        public ActionResult Index()
        {
            return View(new sysdiagramList(_sysdiagramSvc.GetAll()));
        }

        //
        // GET: /sysdiagram/Details/5

        public ActionResult Details(int id = 0)
        {
            sysdiagram sysdiagram = _sysdiagramSvc.GetById(id);
            if (sysdiagram == null)
            {
                return HttpNotFound();
            }
            sysdiagramVm sysdiagramVm = new sysdiagramVm(sysdiagram);





            return View(sysdiagramVm);
        }

        //
        // GET: /sysdiagram/Create

        public ActionResult Create(
        )
        {
                sysdiagram sysdiagram = new sysdiagram();
sysdiagramVm sysdiagramVm = new sysdiagramVm(sysdiagram);
            return View(sysdiagramVm);
        }

        //
        // POST: /sysdiagram/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(sysdiagramVm sysdiagramVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _sysdiagramSvc.Update(sysdiagramVm.sysdiagram);
                return RedirectToAction("Index");
            }

            return View(sysdiagramVm);
        }

        //
        // GET: /sysdiagram/Edit/5

        public ActionResult Edit(int id = 0)
        {
            sysdiagram sysdiagram = _sysdiagramSvc.GetById(id);
            if (sysdiagram == null)
            {
                return HttpNotFound();
            }
            sysdiagramVm sysdiagramVm = new sysdiagramVm(sysdiagram);
            return View(sysdiagramVm);
        }

        //
        // POST: /sysdiagram/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(sysdiagramVm sysdiagramVm)
        {
            if (ModelState.IsValid)
            {
                _sysdiagramSvc.Update(sysdiagramVm.sysdiagram);
                return RedirectToAction("Index");
            }
            return View(sysdiagramVm);
        }

        //
        // GET: /sysdiagram/Delete/5

        public ActionResult Delete(int id = 0)
        {
            sysdiagram sysdiagram = _sysdiagramSvc.GetById(id);
            if (sysdiagram == null)
            {
                return HttpNotFound();
            }
            sysdiagramVm sysdiagramVm = new sysdiagramVm(sysdiagram);
            return View(sysdiagramVm);
        }

        //
        // POST: /sysdiagram/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _sysdiagramSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}