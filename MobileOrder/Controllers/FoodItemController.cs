

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class FoodItemController : Controller
    {
        public FoodItemController(FoodItemSvc foodItemSvc)

        {
            _foodItemSvc = foodItemSvc;
        }
        private FoodItemSvc _foodItemSvc;


        //
        // GET: /FoodItem/

        public ActionResult Index()
        {
            return View(new FoodItemList(_foodItemSvc.GetAll()));
        }

        //
        // GET: /FoodItem/Details/5

        public ActionResult Details(int id = 0)
        {
            FoodItem foodItem = _foodItemSvc.GetById(id);
            if (foodItem == null)
            {
                return HttpNotFound();
            }
            FoodItemVm foodItemVm = new FoodItemVm(foodItem);





            return View(foodItemVm);
        }

        //
        // GET: /FoodItem/Create

        public ActionResult Create(int? FoodMenuId, int? locationId)
        {
            FoodItem foodItem = new FoodItem();
            if (FoodMenuId.HasValue)
            {
                foodItem.FoodMenuId = FoodMenuId.Value;
            }

            FoodItemVm foodItemVm = new FoodItemVm(foodItem);
            return View(foodItemVm);
        }

        //
        // POST: /FoodItem/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FoodItemVm foodItemVm, int? locationId)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _foodItemSvc.Update(foodItemVm.FoodItem);
                if (locationId.HasValue)
                {
                    return RedirectToAction("Index", "FoodMenu", new {locationId=locationId.Value});
                }
            }

            return View(foodItemVm);
        }

        //
        // GET: /FoodItem/Edit/5

        public ActionResult Edit(int id = 0)
        {
            FoodItem foodItem = _foodItemSvc.GetById(id);
            if (foodItem == null)
            {
                return HttpNotFound();
            }
            FoodItemVm foodItemVm = new FoodItemVm(foodItem);
            return View(foodItemVm);
        }

        //
        // POST: /FoodItem/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FoodItemVm foodItemVm, int? locationId)
        {
            if (ModelState.IsValid)
            {
                _foodItemSvc.Update(foodItemVm.FoodItem);
                if (locationId.HasValue)
                {
                    return RedirectToAction("Index", "FoodMenu", new { locationId = locationId.Value });
                }
            }
            return View(foodItemVm);
        }

        //
        // GET: /FoodItem/Delete/5

        public ActionResult Delete(int id = 0)
        {
            FoodItem foodItem = _foodItemSvc.GetById(id);
            if (foodItem == null)
            {
                return HttpNotFound();
            }
            FoodItemVm foodItemVm = new FoodItemVm(foodItem);
            return View(foodItemVm);
        }

        //
        // POST: /FoodItem/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _foodItemSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}