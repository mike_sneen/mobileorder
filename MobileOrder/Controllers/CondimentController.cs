

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class CondimentController : Controller
    {
        public CondimentController(CondimentSvc condimentSvc)

        {
            _condimentSvc = condimentSvc;
        }
        private CondimentSvc _condimentSvc;


        //
        // GET: /Condiment/

        public ActionResult Index()
        {
            return View(new CondimentList(_condimentSvc.GetAll()));
        }

        //
        // GET: /Condiment/Details/5

        public ActionResult Details(int id = 0)
        {
            Condiment condiment = _condimentSvc.GetById(id);
            if (condiment == null)
            {
                return HttpNotFound();
            }
            CondimentVm condimentVm = new CondimentVm(condiment);





            return View(condimentVm);
        }

        //
        // GET: /Condiment/Create

        public ActionResult Create(
            int? FoodItemId        )
        {
                Condiment condiment = new Condiment();
                if (FoodItemId.HasValue)
               {
                    condiment.FoodItemId = FoodItemId.Value;
               }
CondimentVm condimentVm = new CondimentVm(condiment);
            return View(condimentVm);
        }

        //
        // POST: /Condiment/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CondimentVm condimentVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _condimentSvc.Update(condimentVm.Condiment);
                return RedirectToAction("Index");
            }

            return View(condimentVm);
        }

        //
        // GET: /Condiment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Condiment condiment = _condimentSvc.GetById(id);
            if (condiment == null)
            {
                return HttpNotFound();
            }
            CondimentVm condimentVm = new CondimentVm(condiment);
            return View(condimentVm);
        }

        //
        // POST: /Condiment/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CondimentVm condimentVm)
        {
            if (ModelState.IsValid)
            {
                _condimentSvc.Update(condimentVm.Condiment);
                return RedirectToAction("Index");
            }
            return View(condimentVm);
        }

        //
        // GET: /Condiment/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Condiment condiment = _condimentSvc.GetById(id);
            if (condiment == null)
            {
                return HttpNotFound();
            }
            CondimentVm condimentVm = new CondimentVm(condiment);
            return View(condimentVm);
        }

        //
        // POST: /Condiment/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _condimentSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}