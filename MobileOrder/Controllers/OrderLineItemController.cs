

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class OrderLineItemController : Controller
    {
        public OrderLineItemController(OrderLineItemSvc orderLineItemSvc)

        {
            _orderLineItemSvc = orderLineItemSvc;
        }
        private OrderLineItemSvc _orderLineItemSvc;


        //
        // GET: /OrderLineItem/

        public ActionResult Index()
        {
            return View(new OrderLineItemList(_orderLineItemSvc.GetAll()));
        }

        //
        // GET: /OrderLineItem/Details/5

        public ActionResult Details(int id = 0)
        {
            OrderLineItem orderLineItem = _orderLineItemSvc.GetById(id);
            if (orderLineItem == null)
            {
                return HttpNotFound();
            }
            OrderLineItemVm orderLineItemVm = new OrderLineItemVm(orderLineItem);





            return View(orderLineItemVm);
        }

        //
        // GET: /OrderLineItem/Create

        public ActionResult Create(
            int? CustomerOrderId        )
        {
                OrderLineItem orderLineItem = new OrderLineItem();
                if (CustomerOrderId.HasValue)
               {
                    orderLineItem.CustomerOrderId = CustomerOrderId.Value;
               }
OrderLineItemVm orderLineItemVm = new OrderLineItemVm(orderLineItem);
            return View(orderLineItemVm);
        }

        //
        // POST: /OrderLineItem/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderLineItemVm orderLineItemVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _orderLineItemSvc.Update(orderLineItemVm.OrderLineItem);
                return RedirectToAction("Index");
            }

            return View(orderLineItemVm);
        }

        //
        // GET: /OrderLineItem/Edit/5

        public ActionResult Edit(int id = 0)
        {
            OrderLineItem orderLineItem = _orderLineItemSvc.GetById(id);
            if (orderLineItem == null)
            {
                return HttpNotFound();
            }
            OrderLineItemVm orderLineItemVm = new OrderLineItemVm(orderLineItem);
            return View(orderLineItemVm);
        }

        //
        // POST: /OrderLineItem/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OrderLineItemVm orderLineItemVm)
        {
            if (ModelState.IsValid)
            {
                _orderLineItemSvc.Update(orderLineItemVm.OrderLineItem);
                return RedirectToAction("Index");
            }
            return View(orderLineItemVm);
        }

        //
        // GET: /OrderLineItem/Delete/5

        public ActionResult Delete(int id = 0)
        {
            OrderLineItem orderLineItem = _orderLineItemSvc.GetById(id);
            if (orderLineItem == null)
            {
                return HttpNotFound();
            }
            OrderLineItemVm orderLineItemVm = new OrderLineItemVm(orderLineItem);
            return View(orderLineItemVm);
        }

        //
        // POST: /OrderLineItem/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _orderLineItemSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}