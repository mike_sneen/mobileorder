

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class FranchiseController : Controller
    {
        public FranchiseController(FranchiseSvc franchiseSvc)

        {
            _franchiseSvc = franchiseSvc;
        }
        private FranchiseSvc _franchiseSvc;


        //
        // GET: /Franchise/

        public ActionResult Index()
        {
            return View(new FranchiseList(_franchiseSvc.GetAll()));
        }

        //
        // GET: /Franchise/Details/5

        public ActionResult Details(int id = 0)
        {
            Franchise franchise = _franchiseSvc.GetById(id);
            if (franchise == null)
            {
                return HttpNotFound();
            }
            FranchiseVm franchiseVm = new FranchiseVm(franchise);





            return View(franchiseVm);
        }

        //
        // GET: /Franchise/Create

        public ActionResult Create(
        )
        {
                Franchise franchise = new Franchise();
FranchiseVm franchiseVm = new FranchiseVm(franchise);
            return View(franchiseVm);
        }

        //
        // POST: /Franchise/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FranchiseVm franchiseVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _franchiseSvc.Update(franchiseVm.Franchise);
                return RedirectToAction("Index");
            }

            return View(franchiseVm);
        }

        //
        // GET: /Franchise/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Franchise franchise = _franchiseSvc.GetById(id);
            if (franchise == null)
            {
                return HttpNotFound();
            }
            FranchiseVm franchiseVm = new FranchiseVm(franchise);
            return View(franchiseVm);
        }

        //
        // POST: /Franchise/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FranchiseVm franchiseVm)
        {
            if (ModelState.IsValid)
            {
                _franchiseSvc.Update(franchiseVm.Franchise);
                return RedirectToAction("Index");
            }
            return View(franchiseVm);
        }

        //
        // GET: /Franchise/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Franchise franchise = _franchiseSvc.GetById(id);
            if (franchise == null)
            {
                return HttpNotFound();
            }
            FranchiseVm franchiseVm = new FranchiseVm(franchise);
            return View(franchiseVm);
        }

        //
        // POST: /Franchise/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _franchiseSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}