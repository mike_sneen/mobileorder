

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class CustomerController : Controller
    {
        public CustomerController(CustomerSvc customerSvc)

        {
            _customerSvc = customerSvc;
        }
        private CustomerSvc _customerSvc;


        //
        // GET: /Customer/

        public ActionResult Index()
        {
            return View(new CustomerList(_customerSvc.GetAll()));
        }

        //
        // GET: /Customer/Details/5

        public ActionResult Details(int id = 0)
        {
            Customer customer = _customerSvc.GetById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            CustomerVm customerVm = new CustomerVm(customer);





            return View(customerVm);
        }

        //
        // GET: /Customer/Create

        public ActionResult Create(
        )
        {
                Customer customer = new Customer();
CustomerVm customerVm = new CustomerVm(customer);
            return View(customerVm);
        }

        //
        // POST: /Customer/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerVm customerVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _customerSvc.Update(customerVm.Customer);
                return RedirectToAction("Index");
            }

            return View(customerVm);
        }

        //
        // GET: /Customer/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Customer customer = _customerSvc.GetById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            CustomerVm customerVm = new CustomerVm(customer);
            return View(customerVm);
        }

        //
        // POST: /Customer/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerVm customerVm)
        {
            if (ModelState.IsValid)
            {
                _customerSvc.Update(customerVm.Customer);
                return RedirectToAction("Index");
            }
            return View(customerVm);
        }

        //
        // GET: /Customer/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Customer customer = _customerSvc.GetById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            CustomerVm customerVm = new CustomerVm(customer);
            return View(customerVm);
        }

        //
        // POST: /Customer/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _customerSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}