

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels; 

namespace MobileOrder.Controllers
{
    public class RatingController : Controller
    {
        public RatingController(RatingSvc ratingSvc)

        {
            _ratingSvc = ratingSvc;
        }
        private RatingSvc _ratingSvc;


        //
        // GET: /Rating/

        public ActionResult Index()
        {
            return View(new RatingList(_ratingSvc.GetAll()));
        }

        //
        // GET: /Rating/Details/5

        public ActionResult Details(int id = 0)
        {
            Rating rating = _ratingSvc.GetById(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            RatingVm ratingVm = new RatingVm(rating);





            return View(ratingVm);
        }

        //
        // GET: /Rating/Create

        public ActionResult Create(
            int? CustomerOrderId        )
        {
                Rating rating = new Rating();
                if (CustomerOrderId.HasValue)
               {
                    rating.CustomerOrderId = CustomerOrderId.Value;
               }
RatingVm ratingVm = new RatingVm(rating);
            return View(ratingVm);
        }

        //
        // POST: /Rating/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RatingVm ratingVm)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                _ratingSvc.Update(ratingVm.Rating);
                return RedirectToAction("Index");
            }

            return View(ratingVm);
        }

        //
        // GET: /Rating/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Rating rating = _ratingSvc.GetById(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            RatingVm ratingVm = new RatingVm(rating);
            return View(ratingVm);
        }

        //
        // POST: /Rating/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RatingVm ratingVm)
        {
            if (ModelState.IsValid)
            {
                _ratingSvc.Update(ratingVm.Rating);
                return RedirectToAction("Index");
            }
            return View(ratingVm);
        }

        //
        // GET: /Rating/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Rating rating = _ratingSvc.GetById(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            RatingVm ratingVm = new RatingVm(rating);
            return View(ratingVm);
        }

        //
        // POST: /Rating/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _ratingSvc.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}