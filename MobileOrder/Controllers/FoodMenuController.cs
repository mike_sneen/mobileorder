

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileOrder.Models;
using MobileOrder.Services; 
using MobileOrder.ViewModels;
using Newtonsoft.Json;

namespace MobileOrder.Controllers
{
    public class FoodMenuController : Controller
    {
        public FoodMenuController(FoodMenuSvc foodMenuSvc)

        {
            _foodMenuSvc = foodMenuSvc;
        }
        private FoodMenuSvc _foodMenuSvc;


        //
        // GET: /FoodMenu/

        public ActionResult Index(int locationId)
        {
            return View(new FoodMenuList(_foodMenuSvc.GetByLocationId(locationId)){LocationId = locationId});
        }

        public ActionResult GetMenu(int locationId)
        {
            var settings = new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.None
            };

            var jsonResult = new JsonResult
            {
                Data = JsonConvert.SerializeObject(
                    new FoodMenuList(_foodMenuSvc.GetByLocationId(locationId)) {LocationId = locationId}, 
                    settings),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
 
            return Content(jsonResult.Data.ToString(), "application/json");
        }

        //
        // GET: /FoodMenu/Details/5

        public ActionResult Details(int id = 0)
        {
            FoodMenu foodMenu = _foodMenuSvc.GetById(id);
            if (foodMenu == null)
            {
                return HttpNotFound();
            }
            FoodMenuVm foodMenuVm = new FoodMenuVm(foodMenu);





            return View(foodMenuVm);
        }

        //
        // GET: /FoodMenu/Create

        public ActionResult Create(int? locationId, int? foodMenuId)
        {
            var foodMenu = new FoodMenu();
            if (locationId.HasValue)
            {
                foodMenu.LocationId = locationId.Value;
            }
            if (foodMenuId.HasValue)
            {
                foodMenu.FoodMenuId = foodMenuId.Value;
            }
            var foodMenuVm = new FoodMenuVm(foodMenu);
            return View(foodMenuVm);
        }

        //
        // POST: /FoodMenu/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FoodMenuVm foodMenuVm, int? foodMenuId)
        {
#if(DEBUG)
             var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
#endif
            if (ModelState.IsValid)
            {
                if (foodMenuId.HasValue)
                {
                    foodMenuVm.FoodMenu.FoodMenuId = foodMenuId.Value;
                }
                _foodMenuSvc.Update(foodMenuVm.FoodMenu);
                return RedirectToAction("Index", new { locationId = foodMenuVm.FoodMenu.LocationId });
            }

            return View(foodMenuVm);
        }

        //
        // GET: /FoodMenu/Edit/5

        public ActionResult Edit(int id = 0)
        {
            FoodMenu foodMenu = _foodMenuSvc.GetById(id);
            if (foodMenu == null)
            {
                return HttpNotFound();
            }
            FoodMenuVm foodMenuVm = new FoodMenuVm(foodMenu);
            return View(foodMenuVm);
        }

        //
        // POST: /FoodMenu/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FoodMenuVm foodMenuVm)
        {
            if (ModelState.IsValid)
            {
                _foodMenuSvc.Update(foodMenuVm.FoodMenu);
                return RedirectToAction("Index", new { locationId = foodMenuVm.FoodMenu.LocationId });
            }
            return View(foodMenuVm);
        }

        //
        // GET: /FoodMenu/Delete/5

        public ActionResult Delete(int id, int locationId)
        {
            FoodMenu foodMenu = _foodMenuSvc.GetById(id);
            if (foodMenu == null)
            {
                return HttpNotFound();
            }
            FoodMenuVm foodMenuVm = new FoodMenuVm(foodMenu);
            return View(foodMenuVm);
        }

        //
        // POST: /FoodMenu/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int locationId)
        {
            _foodMenuSvc.Delete(id);
            return RedirectToAction("Index", new { locationId = locationId });
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}