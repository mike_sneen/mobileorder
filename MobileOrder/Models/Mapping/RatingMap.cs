using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class RatingMap : EntityTypeConfiguration<Rating>
    {
        public RatingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Rating");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Stars).HasColumnName("Stars");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.CustomerOrderId).HasColumnName("CustomerOrderId");

            // Relationships
            this.HasRequired(t => t.CustomerOrder)
                .WithMany(t => t.Ratings)
                .HasForeignKey(d => d.CustomerOrderId);

        }
    }
}
