using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class CustomerOrderMap : EntityTypeConfiguration<CustomerOrder>
    {
        public CustomerOrderMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.TableInfo)
                .HasMaxLength(50);

            this.Property(t => t.CardName)
                .HasMaxLength(50);

            this.Property(t => t.CardType)
                .HasMaxLength(50);

            this.Property(t => t.AuthCode)
                .HasMaxLength(50);

            this.Property(t => t.ExpirationMonth)
                .HasMaxLength(50);

            this.Property(t => t.ExpirationYear)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CustomerOrder");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.OrderDate).HasColumnName("OrderDate");
            this.Property(t => t.TableInfo).HasColumnName("TableInfo");
            this.Property(t => t.CardName).HasColumnName("CardName");
            this.Property(t => t.CardType).HasColumnName("CardType");
            this.Property(t => t.AuthCode).HasColumnName("AuthCode");
            this.Property(t => t.ExpirationMonth).HasColumnName("ExpirationMonth");
            this.Property(t => t.ExpirationYear).HasColumnName("ExpirationYear");
            this.Property(t => t.SubTotal).HasColumnName("SubTotal");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.Total).HasColumnName("Total");
            this.Property(t => t.CustomerId).HasColumnName("CustomerId");
            this.Property(t => t.LocationId).HasColumnName("LocationId");

            // Relationships
            this.HasRequired(t => t.Customer)
                .WithMany(t => t.CustomerOrders)
                .HasForeignKey(d => d.CustomerId);
            this.HasRequired(t => t.Location)
                .WithMany(t => t.CustomerOrders)
                .HasForeignKey(d => d.LocationId);

        }
    }
}
