using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class FranchiseMap : EntityTypeConfiguration<Franchise>
    {
        public FranchiseMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FranchiseName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Franchise");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FranchiseName).HasColumnName("FranchiseName");
        }
    }
}
