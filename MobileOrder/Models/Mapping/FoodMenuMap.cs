using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class FoodMenuMap : EntityTypeConfiguration<FoodMenu>
    {
        public FoodMenuMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.MenuName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Category)
                .HasMaxLength(50);

            this.Property(t => t.SubCategory)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("FoodMenu");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.MenuName).HasColumnName("MenuName");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.SubCategory).HasColumnName("SubCategory");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.LocationId).HasColumnName("LocationId");
            this.Property(t => t.FoodMenuId).HasColumnName("FoodMenuId");

            // Relationships
            this.HasOptional(t => t.FoodMenuParent)
                .WithMany(t => t.FoodMenus)
                .HasForeignKey(d => d.FoodMenuId);
            this.HasRequired(t => t.Location)
                .WithMany(t => t.FoodMenus)
                .HasForeignKey(d => d.LocationId);

        }
    }
}
