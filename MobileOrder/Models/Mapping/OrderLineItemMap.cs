using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class OrderLineItemMap : EntityTypeConfiguration<OrderLineItem>
    {
        public OrderLineItemMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ItemName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ItemType)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("OrderLineItem");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ItemName).HasColumnName("ItemName");
            this.Property(t => t.Price).HasColumnName("Price");
            this.Property(t => t.ItemType).HasColumnName("ItemType");
            this.Property(t => t.Quantity).HasColumnName("Quantity");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.CustomerOrderId).HasColumnName("CustomerOrderId");
            this.Property(t => t.FoodItemId).HasColumnName("FoodItemId");
            this.Property(t => t.CondimentId).HasColumnName("CondimentId");
            this.Property(t => t.OrderLineItemId).HasColumnName("OrderLineItemId");

            // Relationships
            this.HasOptional(t => t.Condiment)
                .WithMany(t => t.OrderLineItems)
                .HasForeignKey(d => d.CondimentId);
            this.HasRequired(t => t.CustomerOrder)
                .WithMany(t => t.OrderLineItems)
                .HasForeignKey(d => d.CustomerOrderId);
            this.HasOptional(t => t.FoodItem)
                .WithMany(t => t.OrderLineItems)
                .HasForeignKey(d => d.FoodItemId);
            this.HasOptional(t => t.OrderLineItem2)
                .WithMany(t => t.OrderLineItem1)
                .HasForeignKey(d => d.OrderLineItemId);

        }
    }
}
