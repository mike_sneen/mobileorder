using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class BusinessMap : EntityTypeConfiguration<Business>
    {
        public BusinessMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.BusinessName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Business");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.BusinessName).HasColumnName("BusinessName");
            this.Property(t => t.FranchiseId).HasColumnName("FranchiseId");

            // Relationships
            this.HasOptional(t => t.Franchise)
                .WithMany(t => t.Businesses)
                .HasForeignKey(d => d.FranchiseId);

        }
    }
}
