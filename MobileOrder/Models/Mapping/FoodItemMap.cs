using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class FoodItemMap : EntityTypeConfiguration<FoodItem>
    {
        public FoodItemMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FoodItemName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SubCategory)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("FoodItem");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FoodItemName).HasColumnName("FoodItemName");
            this.Property(t => t.Price).HasColumnName("Price");
            this.Property(t => t.SubCategory).HasColumnName("SubCategory");
            this.Property(t => t.FoodItemId).HasColumnName("FoodItemId");
            this.Property(t => t.FoodMenuId).HasColumnName("FoodMenuId");

            // Relationships
            this.HasOptional(t => t.FoodItemParent)
                .WithMany(t => t.FoodItems)
                .HasForeignKey(d => d.FoodItemId);
            this.HasRequired(t => t.FoodMenu)
                .WithMany(t => t.FoodItems)
                .HasForeignKey(d => d.FoodMenuId);

        }
    }
}
