using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class CondimentMap : EntityTypeConfiguration<Condiment>
    {
        public CondimentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CondimentName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SubCategory)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Condiment");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CondimentName).HasColumnName("CondimentName");
            this.Property(t => t.Price).HasColumnName("Price");
            this.Property(t => t.SubCategory).HasColumnName("SubCategory");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.StartChecked).HasColumnName("StartChecked");
            this.Property(t => t.FoodItemId).HasColumnName("FoodItemId");
            this.Property(t => t.CondimentId).HasColumnName("CondimentId");

            // Relationships
            this.HasOptional(t => t.Condiment2)
                .WithMany(t => t.Condiment1)
                .HasForeignKey(d => d.CondimentId);
            this.HasRequired(t => t.FoodItem)
                .WithMany(t => t.Condiments)
                .HasForeignKey(d => d.FoodItemId);

        }
    }
}
