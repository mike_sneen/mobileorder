using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MobileOrder.Models.Mapping
{
    public class LocationMap : EntityTypeConfiguration<Location>
    {
        public LocationMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LocationName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Latitude)
                .HasMaxLength(75);

            this.Property(t => t.Longitude)
                .HasMaxLength(75);

            this.Property(t => t.WiFiName)
                .HasMaxLength(75);

            // Table & Column Mappings
            this.ToTable("Location");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LocationName).HasColumnName("LocationName");
            this.Property(t => t.BusinessId).HasColumnName("BusinessId");
            this.Property(t => t.Latitude).HasColumnName("Latitude");
            this.Property(t => t.Longitude).HasColumnName("Longitude");
            this.Property(t => t.WiFiName).HasColumnName("WiFiName");
        }
    }
}
