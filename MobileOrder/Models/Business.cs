

using System;
using System.Collections.Generic;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class Business   
    {
        public int Id { get; set; }
        public string BusinessName { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public virtual Franchise Franchise { get; set; }
    }
    public class BusinessList : List<Business  > 
    {
        public BusinessList() { }
        public BusinessList(IEnumerable<Business  > items) : base(items) { }
    }
}











































































