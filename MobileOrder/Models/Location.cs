

using System;
using System.Collections.Generic;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class Location   
    {
        public Location()
        {
            this.CustomerOrders = new CustomerOrderList();
            this.FoodMenus = new FoodMenuList();
        }

        public int Id { get; set; }
        public string LocationName { get; set; }
        public int BusinessId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string WiFiName { get; set; }
        public virtual CustomerOrderList CustomerOrders { get; set; }
        public virtual FoodMenuList FoodMenus { get; set; }
    }
    public class LocationList : List<Location  > 
    {
        public LocationList() { }
        public LocationList(IEnumerable<Location  > items) : base(items) { }
    }
}











































































