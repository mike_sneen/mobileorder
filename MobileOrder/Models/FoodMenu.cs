

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class FoodMenu   
    {
        public FoodMenu()
        {
            this.FoodItems = new FoodItemList();
            this.FoodMenus = new FoodMenuList();
        }
        [JsonProperty(PropertyName = "Id")]
        public int Id { get; set; }
        public string MenuName { get; set; }
        [JsonIgnore]
        public string Category { get; set; }
        [JsonIgnore]
        public string SubCategory { get; set; }
        [JsonIgnore]
        public Nullable<int> SortOrder { get; set; }
        [JsonIgnore]
        public int LocationId { get; set; }
        [JsonIgnore]
        public Nullable<int> FoodMenuId { get; set; }
        public virtual FoodItemList FoodItems { get; set; }
        public virtual FoodMenuList FoodMenus { get; set; }
        [JsonIgnore]
        public virtual FoodMenu FoodMenuParent { get; set; }
        [JsonIgnore]
        public virtual Location Location { get; set; }
    }
    public class FoodMenuList : List<FoodMenu>
    {
        [JsonIgnore]
        public int LocationId { get; set; }
        public FoodMenuList() { }
        public FoodMenuList(IEnumerable<FoodMenu> items) : base(items) { }
    }
}











































































