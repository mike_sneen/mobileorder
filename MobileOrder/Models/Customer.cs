

using System;
using System.Collections.Generic;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class Customer   
    {
        public Customer()
        {
            this.CustomerOrders = new CustomerOrderList();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual CustomerOrderList CustomerOrders { get; set; }
    }
    public class CustomerList : List<Customer  > 
    {
        public CustomerList() { }
        public CustomerList(IEnumerable<Customer  > items) : base(items) { }
    }
}











































































