using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using MobileOrder.Models.Mapping;

namespace MobileOrder.Models
{
    public partial class MobileOrderContext : DbContext
    {
        static MobileOrderContext()
        {
            Database.SetInitializer<MobileOrderContext>(null);
        }

        public MobileOrderContext()
            : base("Name=MobileOrderContext")
        {
        }

        public ObjectContext ObjectContext
        {
            get { return (this as IObjectContextAdapter).ObjectContext; }
        }

        public DbSet<Business> Businesses { get; set; }
        public DbSet<Condiment> Condiments { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerOrder> CustomerOrders { get; set; }
        public DbSet<FoodItem> FoodItems { get; set; }
        public DbSet<FoodMenu> FoodMenus { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<OrderLineItem> OrderLineItems { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BusinessMap());
            modelBuilder.Configurations.Add(new CondimentMap());
            modelBuilder.Configurations.Add(new CustomerMap());
            modelBuilder.Configurations.Add(new CustomerOrderMap());
            modelBuilder.Configurations.Add(new FoodItemMap());
            modelBuilder.Configurations.Add(new FoodMenuMap());
            modelBuilder.Configurations.Add(new FranchiseMap());
            modelBuilder.Configurations.Add(new LocationMap());
            modelBuilder.Configurations.Add(new OrderLineItemMap());
            modelBuilder.Configurations.Add(new RatingMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
        }
    }
}
