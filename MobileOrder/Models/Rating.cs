

using System;
using System.Collections.Generic;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class Rating   
    {
        public int Id { get; set; }
        public int Stars { get; set; }
        public string Comment { get; set; }
        public int CustomerOrderId { get; set; }
        public virtual CustomerOrder CustomerOrder { get; set; }
    }
    public class RatingList : List<Rating  > 
    {
        public RatingList() { }
        public RatingList(IEnumerable<Rating  > items) : base(items) { }
    }
}











































































