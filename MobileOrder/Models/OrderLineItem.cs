

using System;
using System.Collections.Generic;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class OrderLineItem   
    {
        public OrderLineItem()
        {
            this.OrderLineItem1 = new OrderLineItemList();
        }

        public int Id { get; set; }
        public string ItemName { get; set; }
        public decimal Price { get; set; }
        public string ItemType { get; set; }
        public int Quantity { get; set; }
        public int SortOrder { get; set; }
        public int CustomerOrderId { get; set; }
        public Nullable<int> FoodItemId { get; set; }
        public Nullable<int> CondimentId { get; set; }
        public Nullable<int> OrderLineItemId { get; set; }
        public virtual Condiment Condiment { get; set; }
        public virtual CustomerOrder CustomerOrder { get; set; }
        public virtual FoodItem FoodItem { get; set; }
        public virtual OrderLineItemList OrderLineItem1 { get; set; }
        public virtual OrderLineItem OrderLineItem2 { get; set; }
    }
    public class OrderLineItemList : List<OrderLineItem  > 
    {
        public OrderLineItemList() { }
        public OrderLineItemList(IEnumerable<OrderLineItem  > items) : base(items) { }
    }
}











































































