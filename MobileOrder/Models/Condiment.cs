

using System;
using System.Collections.Generic;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class Condiment   
    {
        public Condiment()
        {
            this.Condiment1 = new CondimentList();
            this.OrderLineItems = new OrderLineItemList();
        }

        public int Id { get; set; }
        public string CondimentName { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string SubCategory { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public bool StartChecked { get; set; }
        public int FoodItemId { get; set; }
        public Nullable<int> CondimentId { get; set; }
        public virtual CondimentList Condiment1 { get; set; }
        public virtual Condiment Condiment2 { get; set; }
        public virtual FoodItem FoodItem { get; set; }
        public virtual OrderLineItemList OrderLineItems { get; set; }
    }
    public class CondimentList : List<Condiment  > 
    {
        public CondimentList() { }
        public CondimentList(IEnumerable<Condiment  > items) : base(items) { }
    }
}











































































