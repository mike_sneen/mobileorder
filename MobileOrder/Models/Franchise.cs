

using System;
using System.Collections.Generic;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class Franchise   
    {
        public Franchise()
        {
            this.Businesses = new BusinessList();
        }

        public int Id { get; set; }
        public string FranchiseName { get; set; }
        public virtual BusinessList Businesses { get; set; }
    }
    public class FranchiseList : List<Franchise  > 
    {
        public FranchiseList() { }
        public FranchiseList(IEnumerable<Franchise  > items) : base(items) { }
    }
}











































































