

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class FoodItem   
    {
        public FoodItem()
        {
            this.Condiments = new CondimentList();
            this.FoodItems = new FoodItemList();
            this.OrderLineItems = new OrderLineItemList();
        }
        [JsonProperty(PropertyName = "Id")]
        public int Id { get; set; }
        public string FoodItemName { get; set; }
        public decimal Price { get; set; }
        [JsonIgnore]
        public string SubCategory { get; set; }
        [JsonIgnore]
        public Nullable<int> FoodItemId { get; set; }
        [JsonIgnore]
        public int FoodMenuId { get; set; }
        public virtual CondimentList Condiments { get; set; }
        public virtual FoodItemList FoodItems { get; set; }
        [JsonIgnore]
        public virtual FoodItem FoodItemParent { get; set; }
        [JsonIgnore]
        public virtual FoodMenu FoodMenu { get; set; }
        public virtual OrderLineItemList OrderLineItems { get; set; }
    }
    public class FoodItemList : List<FoodItem  > 
    {
        public FoodItemList() { }
        public FoodItemList(IEnumerable<FoodItem  > items) : base(items) { }
    }
}











































































