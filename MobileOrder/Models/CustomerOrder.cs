

using System;
using System.Collections.Generic;

namespace MobileOrder.Models  
{                                                                                                   
    public partial class CustomerOrder   
    {
        public CustomerOrder()
        {
            this.OrderLineItems = new OrderLineItemList();
            this.Ratings = new RatingList();
        }

        public int Id { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string TableInfo { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
        public string AuthCode { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public int CustomerId { get; set; }
        public int LocationId { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Location Location { get; set; }
        public virtual OrderLineItemList OrderLineItems { get; set; }
        public virtual RatingList Ratings { get; set; }
    }
    public class CustomerOrderList : List<CustomerOrder  > 
    {
        public CustomerOrderList() { }
        public CustomerOrderList(IEnumerable<CustomerOrder  > items) : base(items) { }
    }
}











































































