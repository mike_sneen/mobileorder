

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class BusinessSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public BusinessSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  Business GetById(int id) 
        {
            return _mobileOrderContext.Businesses.SingleOrDefault(e => e.Id == id);
        }

        public List<Business> GetAll() 
        {
            return _mobileOrderContext.Businesses.ToList();
        }

        public Business CreateNew()
        {
            return new Business();
        }

        public void Update(Business business)
        {

                _mobileOrderContext.Entry(business).State = business.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            Business business = _mobileOrderContext.Businesses.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<Business>(business).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}