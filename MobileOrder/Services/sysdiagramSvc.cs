

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class sysdiagramSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public sysdiagramSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  sysdiagram GetById(int id) 
        {
            return _mobileOrderContext.sysdiagrams.SingleOrDefault(e => e.Id == id);
        }

        public List<sysdiagram> GetAll() 
        {
            return _mobileOrderContext.sysdiagrams.ToList();
        }

        public sysdiagram CreateNew()
        {
            return new sysdiagram();
        }

        public void Update(sysdiagram sysdiagram)
        {

                _mobileOrderContext.Entry(sysdiagram).State = sysdiagram.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            sysdiagram sysdiagram = _mobileOrderContext.sysdiagrams.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<sysdiagram>(sysdiagram).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}