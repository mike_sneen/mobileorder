

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class FoodItemSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public FoodItemSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  FoodItem GetById(int id) 
        {
            return _mobileOrderContext.FoodItems.SingleOrDefault(e => e.Id == id);
        }

        public List<FoodItem> GetAll() 
        {
            return _mobileOrderContext.FoodItems.ToList();
        }

        public List<FoodItem> GetByFoodMenuId(int foodMenuId)
        {
            List<FoodItem> foodItems =_mobileOrderContext.FoodItems.Where(m=>m.FoodMenuId==foodMenuId && m.FoodItemId == null).ToList();           
            AddChildren(foodItems);
            return foodItems;
        }

        private void AddChildren(List<FoodItem> foodItems)
        {
            foreach (var foodItem in foodItems)
            {
                foodItem.FoodItems = new FoodItemList(GetAll().Where(f=> f.FoodItemId == foodItem.Id).OrderBy(s=>s.FoodItemName).ToList());
                AddChildren(foodItem.FoodItems);
            }
        }

        public FoodItem CreateNew()
        {
            return new FoodItem();
        }

        public void Update(FoodItem foodItem)
        {

                _mobileOrderContext.Entry(foodItem).State = foodItem.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            FoodItem foodItem = _mobileOrderContext.FoodItems.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<FoodItem>(foodItem).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}