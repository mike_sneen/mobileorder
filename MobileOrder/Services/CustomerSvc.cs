

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class CustomerSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public CustomerSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  Customer GetById(int id) 
        {
            return _mobileOrderContext.Customers.SingleOrDefault(e => e.Id == id);
        }

        public List<Customer> GetAll() 
        {
            return _mobileOrderContext.Customers.ToList();
        }

        public Customer CreateNew()
        {
            return new Customer();
        }

        public void Update(Customer customer)
        {

                _mobileOrderContext.Entry(customer).State = customer.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            Customer customer = _mobileOrderContext.Customers.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<Customer>(customer).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}