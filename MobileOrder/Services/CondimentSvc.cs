

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class CondimentSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public CondimentSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  Condiment GetById(int id) 
        {
            return _mobileOrderContext.Condiments.SingleOrDefault(e => e.Id == id);
        }

        public List<Condiment> GetAll() 
        {
            return _mobileOrderContext.Condiments.ToList();
        }

        public Condiment CreateNew()
        {
            return new Condiment();
        }

        public void Update(Condiment condiment)
        {

                _mobileOrderContext.Entry(condiment).State = condiment.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            Condiment condiment = _mobileOrderContext.Condiments.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<Condiment>(condiment).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}