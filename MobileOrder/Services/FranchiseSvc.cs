

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class FranchiseSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public FranchiseSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  Franchise GetById(int id) 
        {
            return _mobileOrderContext.Franchises.SingleOrDefault(e => e.Id == id);
        }

        public List<Franchise> GetAll() 
        {
            return _mobileOrderContext.Franchises.ToList();
        }

        public Franchise CreateNew()
        {
            return new Franchise();
        }

        public void Update(Franchise franchise)
        {

                _mobileOrderContext.Entry(franchise).State = franchise.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            Franchise franchise = _mobileOrderContext.Franchises.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<Franchise>(franchise).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}