

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class CustomerOrderSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public CustomerOrderSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  CustomerOrder GetById(int id) 
        {
            return _mobileOrderContext.CustomerOrders.SingleOrDefault(e => e.Id == id);
        }

        public List<CustomerOrder> GetAll() 
        {
            return _mobileOrderContext.CustomerOrders.ToList();
        }

        public CustomerOrder CreateNew()
        {
            return new CustomerOrder();
        }

        public void Update(CustomerOrder customerOrder)
        {

                _mobileOrderContext.Entry(customerOrder).State = customerOrder.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            CustomerOrder customerOrder = _mobileOrderContext.CustomerOrders.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<CustomerOrder>(customerOrder).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}