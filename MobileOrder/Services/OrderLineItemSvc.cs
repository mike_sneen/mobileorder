

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class OrderLineItemSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public OrderLineItemSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  OrderLineItem GetById(int id) 
        {
            return _mobileOrderContext.OrderLineItems.SingleOrDefault(e => e.Id == id);
        }

        public List<OrderLineItem> GetAll() 
        {
            return _mobileOrderContext.OrderLineItems.ToList();
        }

        public OrderLineItem CreateNew()
        {
            return new OrderLineItem();
        }

        public void Update(OrderLineItem orderLineItem)
        {

                _mobileOrderContext.Entry(orderLineItem).State = orderLineItem.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            OrderLineItem orderLineItem = _mobileOrderContext.OrderLineItems.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<OrderLineItem>(orderLineItem).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}