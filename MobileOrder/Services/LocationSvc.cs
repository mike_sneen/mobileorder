

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class LocationSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public LocationSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  Location GetById(int id) 
        {
            return _mobileOrderContext.Locations.SingleOrDefault(e => e.Id == id);
        }

        public List<Location> GetAll() 
        {
            return _mobileOrderContext.Locations.ToList();
        }

        public Location CreateNew()
        {
            return new Location();
        }

        public void Update(Location location)
        {

                _mobileOrderContext.Entry(location).State = location.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            Location location = _mobileOrderContext.Locations.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<Location>(location).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}