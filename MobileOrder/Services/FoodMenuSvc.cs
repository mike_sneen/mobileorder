

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class FoodMenuSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        private readonly FoodItemSvc _foodItemSvc;

        public FoodMenuSvc(MobileOrderContext mobileOrderContext, FoodItemSvc foodItemSvc)
        {
            _mobileOrderContext = mobileOrderContext;
            _foodItemSvc = foodItemSvc;
        }

        public  FoodMenu GetById(int id) 
        {
            return _mobileOrderContext.FoodMenus.SingleOrDefault(e => e.Id == id);
        }

        public List<FoodMenu> GetAll() 
        {
            return _mobileOrderContext.FoodMenus.ToList();
        }

        public List<FoodMenu> GetByLocationId(int locationId)
        {
            List<FoodMenu> foodMenus = _mobileOrderContext.FoodMenus.Where(l=>l.LocationId == locationId && l.FoodMenuId == null).OrderBy(s=>s.SortOrder).ToList();
            AddChildren(foodMenus);
            return foodMenus;
        }

        private void AddChildren(List<FoodMenu> foodMenus)
        {
            foreach (var foodMenu in foodMenus)
            {
                foodMenu.FoodMenus = new FoodMenuList(GetAll().Where(l => l.FoodMenuId == foodMenu.Id).OrderBy(s=>s.SortOrder).ToList());
                foodMenu.FoodMenus.LocationId = foodMenu.LocationId;
                AddChildren(foodMenu.FoodMenus);
                if (foodMenu.FoodItems.Count < 1)
                {
                    var foodItems = _foodItemSvc.GetByFoodMenuId(foodMenu.Id);
                    var foodItemList = new FoodItemList();
                    foodItemList.AddRange(foodItems);
                    foodMenu.FoodItems = foodItemList;
                }


            }
           
        }

        public FoodMenu CreateNew()
        {
            return new FoodMenu();
        }

        public void Update(FoodMenu foodMenu)
        {

                _mobileOrderContext.Entry(foodMenu).State = foodMenu.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            FoodMenu foodMenu = _mobileOrderContext.FoodMenus.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<FoodMenu>(foodMenu).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}