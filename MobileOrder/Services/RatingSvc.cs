

using System.Collections.Generic;
using System.Linq;
using MobileOrder.Models;

namespace MobileOrder.Services 
{
    public partial class RatingSvc 
    {
        private MobileOrderContext _mobileOrderContext;
        public RatingSvc(MobileOrderContext mobileOrderContext)
        {
            _mobileOrderContext = mobileOrderContext;
        }

        public  Rating GetById(int id) 
        {
            return _mobileOrderContext.Ratings.SingleOrDefault(e => e.Id == id);
        }

        public List<Rating> GetAll() 
        {
            return _mobileOrderContext.Ratings.ToList();
        }

        public Rating CreateNew()
        {
            return new Rating();
        }

        public void Update(Rating rating)
        {

                _mobileOrderContext.Entry(rating).State = rating.Id == 0 ?
                                           System.Data.Entity.EntityState.Added :
                                           System.Data.Entity.EntityState.Modified;

                _mobileOrderContext.SaveChanges();           
        }


        public void Delete(int id) 
        {
            Rating rating = _mobileOrderContext.Ratings.SingleOrDefault(e => e.Id == id);
            _mobileOrderContext.Entry<Rating>(rating).State = System.Data.Entity.EntityState.Deleted;
            _mobileOrderContext.SaveChanges();
        }


    }
}